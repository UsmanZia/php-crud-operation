<?php require_once("../include/Session.php")?>
<?php require_once("../include/basic_connection.php")?>
<?php require_once("../include/Functions.php")?>
<?php $context="admin"?>
<?php include("../include/Layouts/header.php")?>
<?php find_Selected_page();?>
<!-- XXX: Session is start -->
   <div class="row" id="Session">
	  <div class="navigation col-lg-2 ">
	  <!--navigation function call and show the subject and releated pages-->
		   <?php echo Navigation($Current_Subject,$Current_Page);?>    
	 </div>
	 <div class="page_content col-lg-10 bg-success" id="page_style">
	   <?php echo session_message();?>
		<h1>Create New Subject</h1>
		 <form action="create_new_subject.php" method="post" class="form-horizantel">
			<label>Menu Name:</label>
			<input type="text" class="form-control" name="menu_name">
			  <label >Position:</label>
			  <select class="form-control" name="position">
				<?php 
				 $Subject_Set=Find_all_Subject();
				  $count_size=mysqli_num_rows($Subject_Set);
				  for($count=1; $count<=$count_size+1;$count++)
				  {
					  echo "<option value=\"{$count}\">{$count}<option>";
				  }
				
				?>
			  </select>
			  <label >Visible:</label>
			  <div class="radio-inline">
				  <label><input type="radio" name="visible" value="0">No</label>
			  </div>
			  <div class="radio-inline">
				  <label><input type="radio" name="visible" value="1">Yes</label>
			  </div><br>
		       <input type="submit" name="submit" class="btn btn-default" value="Create Subject" /><br><br>
		       <a href="manage_content.php">Cancel</a>
		 </form>
	 </div>
   </div>
       <!-- XXX: Session is ending -->
	<?php include("../include/Layouts/footer.php")?>