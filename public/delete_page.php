<?php require_once("../include/Session.php")?>
<?php require_once("../include/basic_connection.php")?>
<?php require_once("../include/Functions.php")?>
<?php find_Selected_page();?>
<?php
  if(!$Current_Page)
  {
	  redirect_to("https://www.youtube.com/");
  }
?>
<?php 
    $id=$Current_Page["ID"];
	$query="DELETE FROM page";
	$query.=" WHERE ID={$id}";
     $result=mysqli_query($connection,$query);
if($result && mysqli_affected_rows($connection)>=0)
{
	$_SESSION["message"]="Page is Deleted..".$Current_Page["ID"];
	redirect_to("manage_content.php");
}else{
	Query_Set($result);
	redirect_to("manage_admin_user.php");
}
?>
<?php
if(isset($connection)){mysqli_close($connection);}?>