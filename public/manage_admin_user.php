<?php require_once("../include/Session.php")?>
<?php require_once("../include/basic_connection.php")?>
<?php require_once("../include/Functions.php")?>
<?php Confirm_Login()?>
<?php include("../include/Layouts/header.php")?>
 <!-- XXX: Session is start -->
       <div class="row" id="Session">
          <div class="navigation col-lg-2 bg-primary">
          </div>
          <div class="page_content col-lg-10 bg-success" id="page_style">
		  <?php echo session_message()?>
		  <h1>  Manage Admin</h1><br/>
			 <a href="add_new_admin.php" class="btn btn-default">Add Admin</a>
			    <table class="table table-bordered">
				<thead>
			    <th>User Name</th>
			    <th>Action</th>
				</thead>
				<tbody>
				<?php $All_Admin=Find_All_Admin();?>
				  <?php while($Admin=mysqli_fetch_assoc($All_Admin)) {?>
				     <tr>
					  <td><?php echo htmlentities($Admin["UserName"]);?></td>
					 <td><a href="edit_admin.php?id=<?php echo urlencode($Admin["ID"])?>">Edit</a> &nbsp <a href="delete_admin.php?id=<?php echo urlencode($Admin["ID"])?>
				" onclick="return confirm('Are you Sure to delete..');">Detete</a></td>
				    </tr>
				  <?php } ?>
				  </tbody>
                </table>
			  </div>
          </div>
       </div>
       <!-- XXX: Session is ending -->
	<?php include("../include/Layouts/footer.php")?>