<?php require_once("../include/Session.php")?>
<?php require_once("../include/Functions.php")?>
<?php Confirm_Login()?>
<?php $context="admin"?>
<?php include("../include/Layouts/header.php")?>
<!-- XXX: Session is start -->
       <div class="row" id="Session">
          <div class="navigation col-lg-2 bg-primary">
            <p>Navigation bar</p>
          </div>
          <div class="page_content col-lg-10 bg-success" id="page_style">
             <h1>Admin Menu</h1>
              <p>Welcome to admin <?php echo $_SESSION["adminuser"]?>.</p>
              <ul class="nav ">
                <li><a href="manage_content.php">Manage Website content</a></li>
                <li><a href="manage_admin_user.php">Manage admin User</a></li>
                <li><a href="admin_logout.php">Logout</a></li>
              </ul>
          </div>
       </div>
       <!-- XXX: Session is ending -->
	<?php include("../include/Layouts/footer.php")?>