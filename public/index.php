<?php require_once("../include/Session.php")?>
<?php require_once("../include/basic_connection.php")?>
<?php require_once("../include/Functions.php")?>
<?php find_Selected_page(true);?>
<?php include("../include/Layouts/header.php")?>
<?php $context="public"?>

<!-- XXX: Session is start -->
   <div class="row" id="Session">
	  <div class="navigation col-lg-2 ">
	  <!--navigation function call and show the subject and releated pages-->
		   <?php echo Public_Navigation($Current_Subject,$Current_Page);?>    		   
	 </div>
	 <div class="page_content col-lg-10 bg-success" id="page_style">
		 <?php if($Current_Page)
		 {?>
		   <?php echo nl2br(htmlentities($Current_Page["CONTENT"]));?><br> 
		 <?php } else {?>
		    <h1>Welcome !</h1>
		 <?php } ?>
	 </div>
   </div>
       <!-- XXX: Session is ending -->
	<?php include("../include/Layouts/footer.php")?>