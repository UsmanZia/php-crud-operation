<?php require_once("../include/Session.php")?>
<?php require_once("../include/basic_connection.php")?>
<?php require_once("../include/Functions.php")?>
<?php find_Selected_page();?>
<?php
  if(!$Current_Page)
  {
	  redirect_to("manage_content.php");
  }
?>
<?php
 if(isset($_POST['submit']))
 {
	$Id=$Current_Page["ID"];
	$menu_name=mysql_prom($_POST['menu_name']);
	$position=(int)$_POST['position'];
	$visible=(int)$_POST['visible'];
	$content=mysql_prom($_POST['content']);
	$query="UPDATE page SET ";
	$query.="menu_name= '{$menu_name}', ";
	$query.="Position={$position}, ";
	$query.="Visible={$visible} , " ;
	$query.="CONTENT='{$content}' ";
	$query.="WHERE ID={$Id} ";
	$query.="LIMIT 1";
     $result=mysqli_query($connection,$query);
if($result && mysqli_affected_rows($connection)>0)
{
	$_SESSION["message"]="Successfullt Data Update";
	redirect_to("manage_content.php");
}else{
	Query_Set($result);
	redirect_to("manage_content.php");
}
 }
 ?>
<?php $context="admin"?>
<?php include("../include/Layouts/header.php")?>
<!-- XXX: Session is start -->
   <div class="row" id="Session">
	  <div class="navigation col-lg-2 ">
	  <!--navigation function call and show the subject and releated pages-->
		   <?php echo Navigation($Current_Subject,$Current_Page);?>    
	 </div>
	 <div class="page_content col-lg-10 bg-success" id="page_style">
	   <?php echo session_message();?>
		<h1>Edit Page <?php echo htmlentities($Current_Page["menu_name"])?></h1>
		 <form action="edit_page.php?Page=<?php echo urldecode($Current_Page["ID"]);?>" method="post" class="form-horizantel">
			<label>Menu Name:</label>
			<input type="text" class="form-control" name="menu_name" value="<?php echo htmlentities($Current_Page["menu_name"])?>">
			  <label >Position:</label>
			  <select class="form-control" name="position">
				<?php 
				 $Page_Set=Find_Page_From_Subject($Current_Page["Subject_ID"]);
				  $count_size=mysqli_num_rows($Page_Set);
				  for($count=1; $count<=$count_size;$count++)
				  {
					  echo "<option value=\"{$count}\"";
					   if($Current_Page["Position"]==$count)
					   {
						   echo "selected";
					   }
					  echo ">{$count}<option>";
				  }
				
				?>
			  </select>
			  <label >Visible:</label>
			  <div class="radio-inline">
				  <label><input type="radio" name="visible" value="" <?php 
				      if($Current_Page["Visible"]==0) {echo "checked";}?>/>No</label>
			  </div>
			  <div class="radio-inline">
				  <label><input type="radio" name="visible" value="1"<?php 
				      if($Current_Page["Visible"]==1){echo "Checked";}?>/>Yes</label>
			  </div><br>
			  <label>Content:</label>
			   <textarea type="text" value="<?php echo htmlentities($Current_Page["CONTENT"])?>" rows="8" cols="15" name="content" class="form-control"></textarea>
		       <input type="submit" name="submit" class="btn btn-default" value="Edit Page" /><br><br>
		      &nbsp;&nbsp; <a href="manage_content.php">Cancel</a>
		 </form>
	 </div>
   </div>
       <!-- XXX: Session is ending -->
	<?php include("../include/Layouts/footer.php")?>