<?php require_once("../include/Session.php")?>
<?php require_once("../include/basic_connection.php")?>
<?php require_once("../include/Functions.php")?>
<?php find_Selected_page();?>
<?php $context="admin"?>
<?php include("../include/Layouts/header.php")?>
<!-- XXX: Session is start -->
   <div class="row" id="Session">
	  <div class="navigation col-lg-2 ">
	  <!--navigation function call and show the subject and releated pages-->
	        <a href="admin.php" style="text-decoration:none; font-size:20px; margin:5px 9px;">&laquo; Main Menu</a><br/>
		   <?php echo Navigation($Current_Subject,$Current_Page);?>    
            <a  href="new_subject.php" style="color:black;font-size:16px;text-decoration:none;">+ Create New Subject</a>		   
	 </div>
	 <div class="page_content col-lg-10 bg-success" id="page_style">
	      <?php echo session_message()?>
		 <h1>Manage Content</h1>
		 <?php if($Current_Subject)
		 {?>
		   <label>Subject Name: </label>
		   <?php echo htmlentities($Current_Subject["Subject_Name"]);?><br>
		   <label>Position: </label>
		   <?php echo $Current_Subject["Position"];?><br>
		   <label>Visible: </label>
		   <?php echo $Current_Subject["Visible"] == 1 ? "Yes" : "No";?><br>
		    <a href="edit_subject.php?Subject=<?php echo urldecode($Current_Subject["ID"]);?>">+Edit me</a>
		    <a href="delete_subject.php?Subject=<?php echo urldecode($Current_Subject["ID"]);?>" onclick="return confirm('Are you sure to delete');">+Delete</a>
		     <br><br><span style="border-bottom:1px solid black;">__________________</span>
			 <br>
			 <?php 
			     echo Show_pages_By_Subject_ID($Current_Subject["ID"]);
				 
			 ?>
			 <a href="add_new_page.php?Subject=<?php echo urlencode($Current_Subject["ID"])?>">+ Add new Page this Subject</a>
		
		 <?php } else if($Current_Page)
		 {?>
		   <label>Page Name: </label>
		   <?php echo htmlentities($Current_Page["menu_name"]);?><br>
		   <label>Position: </label>
		   <?php echo $Current_Page["Position"];?><br>
		   <label>Visible: </label>
		   <?php echo $Current_Page["Visible"] == 1 ? "Yes" : "No";?><br>
		   <label>Content: </label>
		   <?php echo htmlentities($Current_Page["CONTENT"]);?><br>
		    <a href="edit_page.php?Page=<?php echo $Current_Page["ID"]?>">Edit</a>
		    <a href="delete_page.php?Page=<?php echo $Current_Page["ID"]?>" onclick="return confirm('Are you sure to delete');">Delete</a>
		 <?php } else {?>
		    no data selected;
		 <?php } ?>
	 </div>
   </div>
       <!-- XXX: Session is ending -->
	<?php include("../include/Layouts/footer.php")?>