<?php require_once("../include/Session.php")?>
<?php require_once("../include/basic_connection.php")?>
<?php require_once("../include/Functions.php")?>
<?php find_Selected_page();?>
<?php
  if(!$Current_Subject)
  {
	  redirect_to("https://www.facebook.com/");
  }
?>
<?php $context="admin"?>
<?php include("../include/Layouts/header.php")?>
<!-- XXX: Session is start -->
   <div class="row" id="Session">
	  <div class="navigation col-lg-2 ">
	  <!--navigation function call and show the subject and releated pages-->
		   <?php echo Navigation($Current_Subject,$Current_Page);?>    
	 </div>
	 <div class="page_content col-lg-10 bg-success" id="page_style">
		<h1>Create New Page</h1>
		 <form action="create_new_page.php?Subject=<?php echo urldecode($Current_Subject["ID"]);?>" method="post" class="form-horizantel">
			<label>Menu Name:</label>
			<input type="text" class="form-control" name="menu_name">
			  <label >Position:</label>
			  <select class="form-control" name="position">
				<?php 
				  
				 $Page_Set=Find_Page_From_Subject($Current_Subject["ID"]);
				  $count_size=mysqli_num_rows($Page_Set);
				  for($count=1; $count<=$count_size+1;$count++)
				  {
					  echo "<option value=\"{$count}\">{$count}<option>";
				  }
				
				?>
			  </select>
			  <label >Visible:</label>
			  <div class="radio-inline">
				  <label><input type="radio" name="visible" value="0">No</label>
			  </div>
			  <div class="radio-inline">
				  <label><input type="radio" name="visible" value="1">Yes</label>
			  </div><br>
			  <label>Content</label>
			  <textarea type="text" class="form-control" name="content" rows="10" cols="25">
			  </textarea>
		       <input type="submit" name="submit" class="btn btn-default" value="Create Subject" /><br><br>
		       <a href="manage_content.php">Cancel</a>
		 </form>
	 </div>
   </div>
       <!-- XXX: Session is ending -->
	<?php include("../include/Layouts/footer.php")?>